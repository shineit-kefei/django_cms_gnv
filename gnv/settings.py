import os

DEBUG          = True
TEMPLATE_DEBUG = DEBUG

PROJECT_ROOT = os.path.abspath(os.path.dirname(__name__))

ADMINS = (
    ('chedi toueiti', 'chedi.toueiti@gmail.com'),
)


MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
        'NAME'    : 'gnv',
        'USER'    : 'postgres',
        'PASSWORD': '',
        'HOST'    : 'localhost',
        'PORT'    : '5432',
    }
}

ALLOWED_HOSTS = ["www.gnv.cre8izm.com","ecircle-ag.com","secure.ecircle-ag.com",]

USE_TZ        = True
SITE_ID       = 1
USE_I18N      = True
USE_L10N      = True
TIME_ZONE     = 'Africa/Tunis'
LANGUAGE_CODE = 'fr-fr'

MEDIA_URL   = '/media/'
STATIC_URL  = '/static/'
MEDIA_ROOT  = os.path.abspath(os.path.join(PROJECT_ROOT, "./media" ))
STATIC_ROOT = os.path.abspath(os.path.join(PROJECT_ROOT, "./static"))

STATICFILES_DIRS = (
    os.path.abspath(os.path.join(PROJECT_ROOT, "gnv/static")),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'bfkoua(48zovyo-p@=fr)0!8!q^vyklljoxrma+vk=k7zx#yey'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.doc.XViewMiddleware',

    #'django.middleware.locale.LocaleMiddleware',
    'cms.middleware.multilingual.MultilingualURLMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.csrf',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',

    'cms.context_processors.media',
    'sekizai.context_processors.sekizai',
)


ROOT_URLCONF = 'gnv.urls'

WSGI_APPLICATION = 'gnv.wsgi.application'

TEMPLATE_DIRS = (
    os.path.abspath(os.path.join(PROJECT_ROOT, 'templates')),
    "/home/gnv/public_html/gnvmena/templates/"
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',

    'suit',

    'web',

    'cms',

    'mptt',
    'menus',
    'south',
    'sekizai',
    'debug_toolbar',
    'rest_framework',
    'easy_thumbnails',
    'django.contrib.admin',
    'transmeta',
    'haystack',
    'jsonify',
    'cms_search',
    'cms.plugins.link',
    'cms.plugins.text',
    'cms.plugins.flash',
    #'cms.plugins.picture',
    'cmsplugin_filer_image',
    'cms.plugins.twitter',
    'cms.plugins.snippet',
    'cms.plugins.googlemap',
    'cmsplugin_htmlsitemap',
    'cmsplugin_contact',
    'filer',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


SUIT_CONFIG = {
    'VERSION' : "0.1",

    'ADMIN_NAME': 'GNV admin',
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    'SEARCH_URL': '/admin/auth/user/',
    'MENU_ICONS': {
        'sites': 'icon-leaf',
        'auth': 'icon-lock',
    },
    'MENU_OPEN_FIRST_CHILD': True,

    'MENU': (
        {'app': 'cms', 'label': "Pages management"},
        {'app': 'web', 'label': "Content management"},
        {'app': 'auth', 'label': "Users & Groups"},
        {'app': 'filer', 'label': "Files management"},
        '-',
        {'label': 'Support', 'icon': 'icon-question-sign', 'url': '/support/'},
    ),

    'LIST_PER_PAGE': 30
}

DEBUG_TOOLBAR_CONFIG = {
    'HIDE_DJANGO_SQL'    : False,
    'ENABLE_STACKTRACES' : True,
    'INTERCEPT_REDIRECTS': False,
}

LOCALE_PATHS = (
    os.path.abspath(os.path.join(PROJECT_ROOT, 'locale')),
)

LANGUAGE_CODE = 'fr'
TRANSMETA_DEFAULT_LANGUAGE = 'fr'

gettext = lambda s: s

LANGUAGES = (
    ('fr', gettext('French' )),
    ('ar', gettext('Arabic' )),
)


CMS_LANGUAGES = LANGUAGES
CMS_LANGUAGE_FALLBACK = False
CMS_HIDE_UNTRANSLATED = False

CMS_TEMPLATES = (
    ('template1.html', 'Template One'),
    ('template2.html', 'Template Two'),
)

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.filters',
    'easy_thumbnails.processors.autocrop',
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.scale_and_crop',
)

FILER_STORAGES = {
    'public': {
        'main': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': '/media/public/filer',
                'base_url': '/media/public/filer/',
            },
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': '/media/public/filer_thumbnails',
                'base_url': '/media/public/filer_thumbnails/',
            },
        },
    },
    'private': {
        'main': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': '/media/private/filer',
                'base_url': '/smedia/filer/',
            },
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': '/media/private/filer_thumbnails',
                'base_url': '/smedia/filer_thumbnails/',
            },
        },
    },
}

DEFAULT_FILER_SERVERS = {
    'private': {
        'main': {
            'ENGINE': 'filer.server.backends.default.DefaultServer',
        },
        'thumbnails': {
            'ENGINE': 'filer.server.backends.default.DefaultServer',
        }
    }
}

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    #'debug_toolbar.panels.profiling.ProfilingDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.cache.CacheDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)

INTERNAL_IPS = ('197.0.235.33', '127.0.0.1')

def show_toolbar(request):
    return False

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': show_toolbar,
    'INTERCEPT_REDIRECTS': False,
}

THUMBNAIL_ALIASES = {
    '': {
        's200' : {'size': (200 , 100 ), 'crop': True},
        's300' : {'size': (300 , 300 ), 'crop': True},
        's480' : {'size': (480 , 480 ), 'crop': True},
        's680' : {'size': (680 , 680 ), 'crop': True},
        's1000': {'size': (1000, 1000), 'crop': True},
    },
}

HAYSTACK_SITECONF = 'web.search_sites'
HAYSTACK_SEARCH_ENGINE = 'solr'
HAYSTACK_SOLR_URL = 'http://127.0.0.1:8983/solr'
CMS_MODERATOR = False


THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
