from web.models import *
from django.contrib import admin
from suit.admin import SortableModelAdmin


class DealAdmin(SortableModelAdmin):
    sortable      = 'order'
    list_filter   = ('collection',)
    list_display  = ('title', 'published', 'sidebar')
    list_editable = ('published', 'sidebar')


class NewsAdmin(SortableModelAdmin):
    sortable      = 'order'
    list_display  = ('title', 'published', 'sidebar')
    list_editable = ('published', 'sidebar')


class TopicAdmin(SortableModelAdmin):
    sortable      = 'order'
    list_filter   = ('collection',)
    list_display  = ('title', 'collection', 'published')
    search_fields = ('title', 'collection', 'published')


class DealsCollectionAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display  = ('title', 'deals')

    def deals(self, obj):
        return len(obj.deal_set.all())


class TopicsCollectionAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display  = ('title', 'topics')

    def topics(self, obj):
        return len(obj.topic_set.all())


admin.site.register(Deal            , DealAdmin            )
admin.site.register(News            , NewsAdmin            )
admin.site.register(Topic           , TopicAdmin           )
admin.site.register(DealsCollection , DealsCollectionAdmin )
admin.site.register(TopicsCollection, TopicsCollectionAdmin)
