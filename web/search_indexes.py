from haystack import site
from haystack.indexes import *
from web.models import Deal, Topic, News


class DealIndex(SearchIndex):
    text        = CharField(document=True, use_template=True)
    caption     = CharField(model_attr='caption')
    description = CharField(model_attr='description')
    end_date    = DateTimeField(model_attr='end_date', null=True)
    start_date  = DateTimeField(model_attr='start_date', null=True)

    def index_queryset(self):
        return Deal.objects.filter(published=True)


class TopicIndex(SearchIndex):
    text        = CharField(document=True, use_template=True)
    description = CharField(model_attr='description', null=True)

    def index_queryset(self):
        return Topic.objects.all()


class NewsIndex(SearchIndex):
    text        = CharField(document=True, use_template=True)
    caption     = CharField(model_attr='caption')
    description = CharField(model_attr='description')

    def index_queryset(self):
        return News.objects.filter(published=True)


site.register(Deal , DealIndex)
site.register(News , NewsIndex)
site.register(Topic, TopicIndex)
